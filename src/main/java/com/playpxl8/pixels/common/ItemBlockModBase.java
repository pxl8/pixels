package com.playpxl8.pixels.common;

import com.playpxl8.pixels.common.block.ICustomPlaceBehavior;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

/**
 * Written by @offbeatwitch.
 * Licensed under MIT.
 */
public class ItemBlockModBase extends ItemBlock {
	protected boolean canPlaceBlock = true;

	public ItemBlockModBase(Block block, ResourceLocation res) {
		super(block);
		setRegistryName(res);
	}

	public ItemBlockModBase setCanPlaceBlock(boolean flag) {
		this.canPlaceBlock = flag;
		return this;
	}

	public boolean canPlaceBlock(World worldIn, BlockPos pos) {
		return canPlaceBlock;
	}

	@Override
	public boolean canPlaceBlockOnSide(World worldIn, BlockPos pos, EnumFacing side, EntityPlayer player, ItemStack stack) {
		if (this.block instanceof ICustomPlaceBehavior) {
			ICustomPlaceBehavior block = (ICustomPlaceBehavior) this.block;

			if (block.onPlaceAttempt(worldIn, pos, player, stack))
				return false;
		}

		if (!this.canPlaceBlock(worldIn, pos)) {
			return false;
		}

		return super.canPlaceBlockOnSide(worldIn, pos, side, player, stack);
	}

	@Override
	public boolean placeBlockAt(ItemStack stack, EntityPlayer player, World world, BlockPos pos, EnumFacing side, float hitX, float hitY, float hitZ, IBlockState newState) {
		return super.placeBlockAt(stack, player, world, pos, side, hitX, hitY, hitZ, newState);
	}
}
