package com.playpxl8.pixels.common;

/**
 * Written by @offbeatwitch.
 * Licensed under MIT.
 */
public interface Factory<V> {
	V call();
}
