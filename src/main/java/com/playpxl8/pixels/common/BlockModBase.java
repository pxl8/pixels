package com.playpxl8.pixels.common;

import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;

/**
 * Written by @offbeatwitch.
 * Licensed under MIT.
 */
public class BlockModBase extends Block {
	public BlockModBase(Material materialIn) {
		super(materialIn);
	}

	public BlockModBase(Material blockMaterialIn, MapColor blockMapColorIn) {
		super(blockMaterialIn, blockMapColorIn);
	}
}
