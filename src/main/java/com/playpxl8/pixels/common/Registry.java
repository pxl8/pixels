package com.playpxl8.pixels.common;

import com.google.common.collect.Multimap;
import com.google.common.collect.MultimapBuilder;
import com.playpxl8.pixels.client.IModelOverrides;
import com.playpxl8.pixels.common.tile.ITileBlock;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.registries.IForgeRegistryEntry;

import java.util.HashMap;

/**
 * Written by @offbeatwitch.
 * Licensed under MIT.
 */
public class Registry {
	private Multimap<Class<?>, IForgeRegistryEntry<?>> entries = MultimapBuilder.hashKeys().arrayListValues().build();
	private HashMap<ResourceLocation, IForgeRegistryEntry<?>> instances = new HashMap<>();
	private Side side;

	public Registry(Side side) {
		this.side = side;
	}

	public <T extends IForgeRegistryEntry<T>> void register(IForgeRegistryEntry<T> obj) {
		entries.put(obj.getRegistryType(), obj);
		instances.put(obj.getRegistryName(), obj);
	}

	public void addBlock(BlockModBase block) {
		register(block);

		ItemBlockModBase blockItem = new ItemBlockModBase(block, block.getRegistryName());
		register(blockItem);
	}

	public IForgeRegistryEntry<?> getInstance(ResourceLocation location) {
		return instances.get(location);
	}

	public IForgeRegistryEntry<?> getInstance(String resource) {
		return getInstance(new ResourceLocation(resource));
	}

	@SuppressWarnings("unchecked")
	@SubscribeEvent
	public void onRegistryEvent(RegistryEvent.Register event) {
		Class<?> type = event.getRegistry().getRegistrySuperType();

		if (entries.containsKey(type)) {
			entries.get(type).forEach(entry -> {
				event.getRegistry().register(entry);

				if (entry instanceof ITileBlock) {
					ITileBlock<? extends TileEntity> tileBlock = (ITileBlock) entry;
					Class<? extends TileEntity> clazz = tileBlock.getTileClass();

					GameRegistry.registerTileEntity(clazz, entry.getRegistryName().toString());
				}

				if (side.isClient()) {
					if (entry instanceof IModelOverrides) {
						((IModelOverrides) entry).applyModelOverrides();
						return;
					}

					if (entry instanceof ItemBlock) {
						ItemBlock item = (ItemBlock) entry;
						ResourceLocation resource = item.getRegistryName();
						if (resource == null) resource = item.getBlock().getRegistryName();
						if (resource == null) return;

						ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(resource, "inventory"));
					} else if (entry instanceof Item) {
						Item item = (Item) entry;
						ResourceLocation resource = item.getRegistryName();
						if (resource == null) return;

						ModelLoader.setCustomModelResourceLocation(item, 0, new ModelResourceLocation(resource, "inventory"));
					}
				}
			});
		}
	}
}
