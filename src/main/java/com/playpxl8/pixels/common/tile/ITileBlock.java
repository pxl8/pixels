package com.playpxl8.pixels.common.tile;

import com.playpxl8.pixels.common.Factory;
import net.minecraft.tileentity.TileEntity;

/**
 * Written by @offbeatwitch.
 * Licensed under MIT.
 */
public interface ITileBlock<V extends TileEntity> {
	Class<V> getTileClass();
	Factory<V> getTileFactory();
}
