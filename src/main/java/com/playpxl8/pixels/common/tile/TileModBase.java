package com.playpxl8.pixels.common.tile;

import net.minecraft.block.state.IBlockState;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.play.server.SPacketUpdateTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;

import javax.annotation.Nullable;

/**
 * Written by @offbeatwitch.
 * Licensed under MIT.
 */
public class TileModBase extends TileEntity {
	@Override
	public void onDataPacket(NetworkManager net, SPacketUpdateTileEntity pkt) {
		this.readFromNBT(pkt.getNbtCompound());

		BlockPos pos = pkt.getPos();
		IBlockState state = world.getBlockState(pos);
		world.notifyBlockUpdate(pkt.getPos(), state, state, 1);
	}

	@Nullable
	@Override
	public SPacketUpdateTileEntity getUpdatePacket() {
		NBTTagCompound nbt = this.serializeNBT();

		return new SPacketUpdateTileEntity(this.pos, this.getBlockMetadata(), nbt);
	}

	@Override
	public NBTTagCompound getUpdateTag() {
		return this.serializeNBT();
	}
}
