package com.playpxl8.pixels.common.block;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Written by @offbeatwitch.
 * Licensed under MIT.
 */
public interface ICustomPlaceBehavior {
	/**
	 * Called when the player attempts to place this block's ItemBlock.
	 *
	 * @param world World the attempt was made in.
	 * @param pos Position corresponding to the block this block was placed *against*
	 * @param player Player that placed the block
	 * @param stack Held stack causing the block place.
	 * @return True to indicate success, meaning custom placement behavior has been put in place. If false is returned, checks continue as normal.
	 */
	@SideOnly(Side.CLIENT)
	boolean onPlaceAttempt(World world, BlockPos pos, EntityPlayer player, ItemStack stack);
}
