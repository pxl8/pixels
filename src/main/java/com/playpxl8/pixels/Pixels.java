package com.playpxl8.pixels;

import net.minecraftforge.fml.common.Mod;

@Mod(modid = Pixels.MOD_ID, name = Pixels.MOD_NAME)
public class Pixels {
	public static final String MOD_ID = "pixels";
	public static final String MOD_NAME = "Pixels";
}
