package com.playpxl8.pixels.modular;

/**
 * Written by @offbeatwitch.
 * Licensed under MIT.
 */
public interface IModuleChecker {
	boolean isModuleEnabled(String name);
}
