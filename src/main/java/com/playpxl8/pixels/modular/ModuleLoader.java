package com.playpxl8.pixels.modular;

import com.playpxl8.pixels.common.Registry;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

import java.util.ArrayList;

/**
 * Written by @offbeatwitch.
 * Licensed under MIT.
 */
public class ModuleLoader {
	private ArrayList<IModule> modules = new ArrayList<>();
	private IModuleChecker checker;

	public ModuleLoader(IModuleChecker checker) {
		this.checker = checker;
	}

	public void addModule(IModule module) {
		String name = module.getName();

		if (checker.isModuleEnabled(name)) {
			this.modules.add(module);
		}
	}

	public void register(Registry registry) {
		this.modules.forEach(module -> module.onRegister(registry));
	}

	public void preInit(FMLPreInitializationEvent event) {
		this.modules.forEach(module -> module.onPreInit(event));
	}
}
