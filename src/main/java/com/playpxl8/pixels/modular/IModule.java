package com.playpxl8.pixels.modular;

import com.playpxl8.pixels.common.Registry;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
 * Written by @offbeatwitch.
 * Licensed under MIT.
 */
public interface IModule {
	String getName();
	void onRegister(Registry registry);
	void onPreInit(FMLPreInitializationEvent event);
}
