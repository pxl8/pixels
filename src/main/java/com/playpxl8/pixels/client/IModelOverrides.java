package com.playpxl8.pixels.client;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

/**
 * Written by @offbeatwitch.
 * Licensed under MIT.
 */
public interface IModelOverrides {
	@SideOnly(Side.CLIENT)
	void applyModelOverrides();
}
